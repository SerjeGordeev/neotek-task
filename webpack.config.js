const webpack = require("webpack");
const _ = require("lodash");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const PROXY = process.env.PROXY;
const NODE_ENV = process.env.NODE_ENV;

const dirAliases = {
	main: __dirname + "/app/main",
	components: __dirname + "/app/components",
	npm: __dirname + "/node_modules",
	images: __dirname + "/app/images",
	styles: __dirname + "/app/styles"
};

module.exports = {
	context: __dirname + "/app",
	resolve: {
		alias: _.assign({}, dirAliases)
	},
	entry: "./main/app.js",
	output: {
		path: __dirname + "/build",
		filename: "./[name].js"
	},
	module: {
		loaders: [
			{test: /\.js$/, loader: "babel-loader"},
			{test: /\.html$/, loader: "raw-loader"},
			{test: /\.css$/, loader: "style-loader!css-loader"},
			{test: /\.scss$/, loader: "style-loader!css-loader!sass-loader"},
			{test: /\.(png|jpg|gif|svg)$/, loader: "url-loader"}
		]
	},
	devServer: {
		port: 8080,
		contentBase: "./build",
		proxy: {
			"**/api/**": {
				target: (PROXY === "localhost")?"http://localhost:8780":"http://neotek-task.herokuapp.com/",
				changeOrigin: true,
				secure: false,
				toProxy: true
			},
			"/attachments/**": {
				target: (PROXY === "localhost")?"http://localhost:8780":"http://neotek-task.herokuapp.com/",
				changeOrigin: true,
				secure: false,
				toProxy: true
			}
		},
		historyApiFallback: {
			index: "/"
		}
	}
};

if(NODE_ENV == "production"){
	module.exports.plugins = [
		new webpack.optimize.CommonsChunkPlugin({
			name: "vendor",
			minChunks: function (module) {
				return module.context && module.context.indexOf("node_modules") !== -1;
			}
		}),
		new UglifyJSPlugin({
			compress:{
				sequences: true,
				dead_code: true,
				conditionals: true,
				booleans: true,
				unused: true,
				if_return: true,
				join_vars: true,
				drop_console: true,
				unsafe: true
			},
			mangle: false
		})
	];
}