import angular from "angular";
import "angular-ui-router";
import "restangular";
import "ng-file-upload"
import "normalize.css";
import "styles/common.scss";

import "components/movies";
import "components/genres";
import "components/actors";
import "components/countries";
import "components/directors";
import "components/common";
import "components/backend";

import "@iamadamjowett/angular-click-outside/clickoutside.directive.js";

const App = angular.module("neotek", [
	"ui.router",
	"neotek.movies",
	"neotek.common",
	"neotek.backend",
	"neotek.genres",
	"neotek.actors",
	"neotek.countries",
	"neotek.directors",
	"restangular",
	"angular-click-outside",
	"ngFileUpload"
]);

App.config(function ($stateProvider, $locationProvider, $urlRouterProvider, flashAlertProvider) {
	$urlRouterProvider.otherwise("/movies/list");
	flashAlertProvider.setAlertTime(2000);
});

App.run(($rootScope, $state, $document)=>{

});