import template from "./genres-list.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "genresList",
	template
})
class GenresListComponent {

	/* @ngInject */
	constructor($state, Genres) {
		this.services = {$state, Genres};
		this.genres = [];
		this.editableListOptions = {
			modelName: "name",
			placeholder: "Название жанра",
			getLabel(item){
				return item.name;
			}
		};
	}

	$onInit() {
		this.getGenresList();
	}

	getGenresList(){
		const {Genres} = this.services;
		this.promise = Genres.api().getList().then(genres=>{
			this.genres = genres;
		});
	}

	addNew(){
		const {Genres} = this.services;
		this.promise = Genres.api().post({}).then(genre=>{
			this.genres.push(genre);
		});
	}

	updateGenre(genre){
		this.promise = genre.put().then();
	}

	removeGenre(data){
		this.promise = data.item.remove().then(()=>{
			this.genres.splice(data.ix, 1);
		});
	}
}

export {GenresListComponent};
