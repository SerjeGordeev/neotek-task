import "./styles.scss";
import _ from "lodash";
import template from "./app-main-layout.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "appMainLayout",
	template,
	transclude: true
})
class AppMainLayoutComponent {

	/* @ngInject */
	constructor(Movies) {
		this.services = {Movies};
	}

}

export {AppMainLayoutComponent};
