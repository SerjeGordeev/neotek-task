import "./styles.scss";
import _ from "lodash";
import template from "./app-select.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "appSelect",
	bindings: {
		options: "<",
		ngModel: "<",
		ngDisabled: "<?",
		ngRequired: "<?",

		placeholder: "@",

		/* spec options*/
		optionsList: "<?",
		getLabel: "<?",
		enableAll: "<?",
		enableSearch: "<?",

		//callback
		onChange: "&"
	},
	template
})
class AppSelectComponent {
	/**
	 * @ngInject
	 */
	constructor($scope, $timeout, $window, $element) {
		this.services = {$scope, $timeout, $window, $element};

		this.showList = false;
		this.searchString = null;
		this.optionsCollection = [];
		this.selectedCount = 0;

		this.defaultOptions = {
			getLabel: this.getLabel || ((optionData)=> {
				return String(optionData)
			}),
			enableAll: this.enableAll || false,
			enableSearch: this.enableSearch || false,
			multiple: false,
			placeholder: this.placeholder || null
		};
	}

	$onInit() {
		const {$scope, $window} = this.services;
		this.$$options = _.assign({}, this.defaultOptions, this.options || {});
		this.getOptionsCollection();
		this.$$searchedStringFilter = this.$$options.enableSearch ? this.searchedStringFilter.bind(this) : (()=>true);
		if (!this.ngModel) {
			this.ngModel = this.$$options.multiple ? [] : null;
		}

		if (this.$$options.multiple) {
			this.$$selectOption = this.toggleOptionSelection.bind(this);
			this.$$synchronizeOptions = this.synchronizeMultipleOptions.bind(this);
		} else {
			this.$$selectOption = this.selectOption.bind(this);
			this.$$synchronizeOptions = this.synchronizeNotMultipleOptions.bind(this);
		}

		this.$$checkPosition = this.checkPosition.bind(this);

		this.resizeListener = angular.element($window).on("resize", this.$$checkPosition);
		this.scrollListener = angular.element($window).on("scroll", this.$$checkPosition);

		$scope.$watch(() => this.ngModel, (nV) => {
			this.$$synchronizeOptions(nV);
		}, true);
	}

	$onChanges(changes) {
		if (changes.optionsList && !changes.optionsList.isFirstChange()) {
			this.synchronizeOptionsList(changes.optionsList.currentValue);
		}

		if (changes.ngModel) {
			this.$checkIsAllSelected();
		}
	}

	$onDestroy() {
		this.resizeListener.unbind("resize", this.$$checkPosition);
		this.scrollListener.unbind("scroll", this.$$checkPosition);
	}

	$checkIsAllSelected() {
		if (!this.multiple) {
			return;
		}
		this.isSelectedAll = ((this.ngModel.length > 0) && (this.ngModel.length === this.optionsCollection.length));
	}

	checkPosition() {
		if (this.showList) {
			const {$window, $element, $timeout} = this.services;
			const dropdownElem = $element[0].querySelector(".select-dropdown");

			$timeout(() => {
				const dropdownElemHeight = parseInt(getComputedStyle(dropdownElem).height, 10);

				if ($window.innerHeight < $element[0].getBoundingClientRect().top + dropdownElemHeight * 1.5) {
					this.$$options.overVerticalLimit = true;
				} else {
					this.$$options.overVerticalLimit = false;
				}
			});
		}
	}

	/**
	 * Проверка одинаковости элементов
	 *
	 * @param items
	 * @returns {boolean}
	 */
	$isEqualItems(...items) {
		const itemsKeys = _.chain(items)
			.map((item) => {
				if (this.$$options.getKey) {
					return this.$$options.getKey(item);
				}

				return this.$$options.getLabel(item);
			})
			.uniq()
			.compact()
			.value();

		return itemsKeys.length === 1;
	}

	/* SIDE EFFECTS REACTION FUNCTIONS */

	synchronizeOptionsList(nV) {
		const newOptionsCollection = [];

		_.forEach(nV, (item) => {
			const existedOption = _.find(this.optionsCollection, (option) => this.$isEqualItems(option.data, item));
			if (existedOption) {
				newOptionsCollection.push(existedOption);
			} else {
				newOptionsCollection.push({
					data: item,
					selected: false
				});
			}
		});

		this.optionsCollection = newOptionsCollection;

		if (this.$$options.multiple) {
			this.updateMultipleNgModel();
		} else {
			this.updateNotMultipleSelectedValue();
			this.updateNotMultipleNgModel();
		}
	}

	updateMultipleNgModel() {
		let selectedOptions = _.filter(this.optionsCollection, {selected: true});
		this.onChange({model: _.map(selectedOptions, "data")});
	}

	updateNotMultipleNgModel() {
		let selectedOption = _.find(this.optionsCollection, {selected: true});
		this.onChange({model: _.get(selectedOption, "data", null)});
	}

	updateNotMultipleSelectedValue() {
		let selectedOption = _.find(this.optionsCollection, {selected: true});
		this.searchString = selectedOption ? this.$$options.getLabel(selectedOption.data) : null;
	}

	synchronizeMultipleOptions(ngModel) {
		_.forEach(this.optionsCollection, (option) => {
			const exists = _.find(ngModel, (value) => {
				return this.$isEqualItems(option.data, value);
			});
			option.selected = Boolean(exists);
		});
	}

	synchronizeNotMultipleOptions(nV) {
		_.forEach(this.optionsCollection, option => {
			option.selected = this.$isEqualItems(option.data, nV);
		});

		this.updateNotMultipleSelectedValue();
	}

	/* /SIDE EFFECTS REACTION FUNCTIONS */

	searchedStringFilter(option) {
		return this.searchString ? _.includes(this.$$options.getLabel(option.data), this.searchString) : true;
	}

	toggleShowMode() {
		this.showList = !this.showList;
		this.$$checkPosition();
	}

	getOptionsCollection() {
		_.forEach(this.optionsList, (optionData)=> {
			this.optionsCollection.push({
				data: optionData,
				selected: false
			});
		});
	}

	//for multiple
	toggleOptionSelection(option, ev) {
		ev.stopPropagation();
		if (this.ngDisabled) {
			return;
		}
		option.selected = !option.selected;
		if (option.selected) {
			this.ngModel.push(option.data);
			this.selectedCount += 1;
		} else {
			const ix = _.findIndex(this.ngModel, (optionData) => {
				return this.$isEqualItems(option.data, optionData);
			});
			this.ngModel.splice(ix, 1);
			this.selectedCount -= 1;
			this.isSelectedAll = false;
		}
		
		this.onChange({model: this.ngModel});
	}

	//for not multiple
	selectOption(option, ev) {
		if (this.ngDisabled) {
			return;
		}
		//this.ngModel = option.data;
		this.onChange({model: option.data});

		this.optionsCollection.forEach((option) => {
			option.selected = false;
		});

		option.selected = true;
		this.searchString = this.$$options.getLabel(option.data);
	}

	onOutsideClick() {
		if (this.showList) {
			this.showList = false;
		}
	}

	onInputFocus() {
		if (!this.$$options.multiple && _.filter(this.optionsCollection, {selected: true}).length) {
			this.searchString = null;
			this.onChange({model: null});
			this.$clearSelectedOptions();
			//this.showList = true;
		}
	}

	selectAll() {
		const result = [];

		this.isSelectedAll = !this.isSelectedAll;

		if (this.isSelectedAll) {
			_.forEach(this.optionsCollection, (option) => {
				option.selected = true;
				result.push(option.data);
				this.ngModel.push(option.data);
			});
			this.selectedCount = this.optionsCollection.length;
		} else {
			this.$clearSelectedOptions();
			this.ngModel.splice(0, this.ngModel.length);
			result.splice(0, this.ngModel.length);
			this.selectedCount = 0;
		}

		this.onChange({model: result});
	}

	$clearSelectedOptions() {
		this.optionsCollection
			.forEach((option) => {
				option.selected = false;
			});
	}
}

export {AppSelectComponent };