import "./styles.scss";
import _ from "lodash";
import template from "./app-array-editor.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "appArrayEditor",
	template,
	bindings: {
		
	}
})
class AppEditorComponent {

	/* @ngInject */
	constructor() {

	}

}

export {AppEditorComponent};
