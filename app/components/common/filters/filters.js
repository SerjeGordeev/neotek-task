/* @ngInject */
function HighlightFilter($sce) {
	return (text, phrase) => {
		let result = phrase?
			text.replace(new RegExp("(" + phrase + ")", "gi"), "<span class='highlighted'>$1</span>")
			:text;

		return $sce.trustAsHtml(result);
	};
}

export { HighlightFilter };