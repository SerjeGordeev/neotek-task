import "./styles.scss";
import template from "./app-navbar.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "appNavbar",
	template
})
class AppNavbarComponent {

	/* @ngInject */
	constructor() {
		this.routes = [
			{
				state: "movies.list",
				title: "Фильмы"
			},
			{
				state: "genres.list",
				title: "Жанры"
			},
			{
				state: "countries.list",
				title: "Страны"
			},
			{
				state: "actors.list",
				title: "Актеры"
			},
			{
				state: "directors.list",
				title: "Режиссеры"
			}
		];
	}

}

export {AppNavbarComponent};
