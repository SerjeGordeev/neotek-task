import { SvgIconComponent }         from "./svg-icon/svg-icon.component.js";
import { AppHeaderComponent }       from "./app-header/app-header.component.js";
import { AppFooterComponent }       from "./app-footer/app-footer.component.js";
import { AppMainLayoutComponent }   from "./app-main-layout/app-main-layout.component.js";
import { AppEditorComponent }       from "./app-array-editor/app-array-editor.components.js";
import { AppSelectComponent }       from "./app-select/app-select.component.js";
import { AppCheckboxComponent }     from "./app-checkbox/app-checkbox.component.js";
import { EditableListComponent }    from "./editable-list/editable-list.component.js";
import { AppNavbarComponent }       from "./app-navbar/app-navbar.component.js";
import { AjaxLoaderComponent }      from "./ajax-loader/ajax-loader.component.js";

import { HighlightFilter } from "./filters/filters.js";

angular
	.module("neotek.common",[])
	.filter("highlight", HighlightFilter)
	.component(SvgIconComponent.selector,       SvgIconComponent)
	.component(AppHeaderComponent.selector,     AppHeaderComponent)
	.component(AppFooterComponent.selector,     AppFooterComponent)
	.component(AppMainLayoutComponent.selector, AppMainLayoutComponent)
	.component(AppEditorComponent.selector,     AppEditorComponent)
	.component(AppSelectComponent.selector,     AppSelectComponent)
	.component(AppCheckboxComponent.selector,   AppCheckboxComponent)
	.component(EditableListComponent.selector,  EditableListComponent)
	.component(AppNavbarComponent.selector,     AppNavbarComponent)
	.component(AjaxLoaderComponent.selector,    AjaxLoaderComponent);

require("./alert-flash")
