class MoviesService {
	/* @ngInject */
	constructor(Genres, Actors, Countries, Directors, Attachments, Api, $q){
		this.services = {Genres, Actors, Countries, Directors, Attachments, Api, $q};
		this.apiUrl = "/movies";
	}

	api() {
		const {Api} = this.services;

		return Api.service(this.apiUrl);
	}

	getMovies(params){
		return this.api().getList(params).then(movies=>{
			return this.$$processMovies(movies);
		});
	}

	getOne(id){
		return this.api().one(id).get().then(movie=>{
			return this.$$processMovies([movie]);
		}).then(movies=>{
			return movies[0];
		});
	}

	$$processMovies(movies){
		const {$q, Genres, Actors, Countries, Directors, Attachments} = this.services;

		let genresIds = _.chain(movies).map("genresIds").flatten().compact().value();
		let actorsIds = _.chain(movies).map("actorsIds").flatten().compact().value();
		let countriesIds = _.chain(movies).map("countriesIds").flatten().compact().value();
		let filmDirectorsIds = _.chain(movies).map("filmDirectorsIds").flatten().compact().value();
		let previewsIds = _.chain(movies).map("previewImageId").flatten().compact().value();

		return $q.all({
			genres: Genres.api().getList({ids: genresIds.join(",")}),
			actors: Actors.api().getList({ids: actorsIds.join(",")}),
			countries: Countries.api().getList({ids: countriesIds.join(",")}),
			filmDirectors: Directors.api().getList({ids: filmDirectorsIds.join(",")}),
			previews: Attachments.api().getList({ids: previewsIds.join(",")})
		}).then(response=>{
			_.map(movies, movie=>{
				movie.genres = _.filter(response.genres, item=> movie.genresIds.includes(item.id));
				movie.actors = _.filter(response.actors, item=> movie.actorsIds.includes(item.id));
				movie.countries = _.filter(response.countries, item=> movie.countriesIds.includes(item.id));
				movie.filmDirectors = _.filter(response.filmDirectors, item=> movie.filmDirectorsIds.includes(item.id));
				movie.previewImage = _.chain(response.previews).find({id: movie.previewImageId}).get("url").value();
			});
			return movies;
		})
	}
}
export {MoviesService};