class DirectorsService {
	/* @ngInject */
	constructor(Api, $q){
		this.services = {Api, $q};
		this.apiUrl = "/directors";
	}

	api() {
		const {Api} = this.services;

		return Api.service(this.apiUrl);
	}

}
export {DirectorsService};