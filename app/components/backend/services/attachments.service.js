class AttachmentsService {
	/* @ngInject */
	constructor(Api, Upload){
		this.services = {Api, Upload};
		this.apiUrl = "/attachments";
	}

	api() {
		const {Api} = this.services;

		return Api.service(this.apiUrl);
	}

	upload(file){
		const {Upload} = this.services;
		return Upload.upload({
			url: "api/"+this.apiUrl,
			data: {file}
		}).then((response)=>{
			return response.data;
		})
	}

}
export {AttachmentsService};