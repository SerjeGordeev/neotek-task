import { MoviesService } from "./services/movies.service.js";
import { ActorsService } from "./services/actors.service.js";
import { CountriesService } from "./services/countries.service.js";
import { DirectorsService } from "./services/directors.service.js";
import { GenresService } from "./services/genres.service.js";
import { AttachmentsService } from "./services/attachments.service.js";
import { MovieTemplate } from "./factories/movie.factory.js";
import { Api } from "./wrappers/Api.js";

angular
	.module("neotek.backend",[])
	.service("Movies", MoviesService)
	.service("Actors", ActorsService)
	.service("Countries", CountriesService)
	.service("Directors", DirectorsService)
	.service("Genres", GenresService)
	.service("Attachments", AttachmentsService)
	.factory("Api", Api)
	.factory("MovieTemplate", MovieTemplate);
