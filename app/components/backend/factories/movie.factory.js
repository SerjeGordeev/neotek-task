
function MovieTemplate() {
	const self = this;
	self.movieTemplate = {
		name: "",
		filmDirectors: [],
		genres: [],
		rating: null,
		actors: [],
		previewImage: "",
		description: "",
		year: null,
		countries: [],
		budget: ""
	};

	return {
		getTemplate(){
			return angular.copy(self.movieTemplate);
		}
	}
}

export { MovieTemplate };