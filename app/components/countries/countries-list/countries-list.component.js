import template from "./countries-list.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "countriesList",
	template
})
class CountriesListComponent {

	/* @ngInject */
	constructor($state, Countries) {
		this.services = {$state, Countries};
		this.countries = [];
		this.editableListOptions = {
			modelName: "name",
			placeholder: "Название страны",
			getLabel(item){
				return item.name;
			}
		};
	}

	$onInit() {
		this.getCountryList();
	}

	getCountryList(){
		const {Countries} = this.services;
		this.promise = Countries.api().getList().then(countries=>{
			this.countries = countries;
		});
	}

	addNew(){
		const {Countries} = this.services;
		this.promise = Countries.api().post({}).then(actor=>{
			this.countries.push(actor);
		});
	}

	updateCountry(actor){
		this.promise = actor.put().then();
	}

	removeCountry(data){
		this.promise = data.item.remove().then(()=>{
			this.countries.splice(data.ix, 1);
		});
	}
}

export {CountriesListComponent};
