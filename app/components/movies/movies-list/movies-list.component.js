import "./styles.scss";
import template from "./movies-list.template.html";
import {Component} from "main/decorators";

import addIcon from "images/ic_add_black_24px.svg";

@Component({
	selector: "moviesList",
	template
})
class MoviesListComponent {

	/* @ngInject */
	constructor($state, Movies) {
		this.services = {$state, Movies};
		this.icons = {addIcon};
		this.movies = [];
	}

	$onInit() {
		const {Movies} = this.services;

		this.promise = Movies.getMovies().then(movies=>{
			this.movies = movies;
			console.log(this.movies)
		});
	}

	openDetail(movie){
		const {$state} = this.services;
		$state.transitionTo("movies.detail", {
			id: movie.id,
			movieData: movie
		});
	}

	removeMovie(movie, ix){
		this.promise = movie.remove().then(()=>{
			this.movies.splice(ix, 1)
		});
	}
}

export {MoviesListComponent};
