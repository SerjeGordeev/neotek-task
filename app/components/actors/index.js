import {ActorsListComponent }  from "./actors-list/actors-list.component.js";

angular
	.module("neotek.actors",[])
	.component(ActorsListComponent.selector, ActorsListComponent)
	.config(route);

function route($stateProvider) {

	$stateProvider.state({
		name: 'actors',
		url: '/actors',
		template: `<ui-view></ui-view>`,
		onEnter: function(){

		}
	});

	$stateProvider.state({
		name: 'actors.list',
		url: '/list',
		template: `<actors-list></actors-list>`,
		onEnter: function(){

		}
	});
}